//Sidebar menu open - close + arrow animation
const homeSideBar = document.querySelector(".home-side-bar");
homeSideBar.addEventListener("click", function (event) {
  const cardArrow = event.target.closest(".side-menu-arrow");
  if (cardArrow) {
    homeSideBar.classList.toggle("closed");
  }
});



const navArrow = document.querySelectorAll(".nav-arrow");
const navOpen = document.querySelector(".nav");

function toggleNavMenu() {
  navOpen.classList.toggle("is-open");
  document.querySelector(".overlay").classList.toggle("is-open");
}

navArrow.forEach((navArrow) => {
  navArrow.addEventListener("click", toggleNavMenu);
});
